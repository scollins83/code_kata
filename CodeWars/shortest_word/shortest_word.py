import unittest


class MyTestCase(unittest.TestCase):
    def test_single_letter_word(self):
        test_string = "a"
        self.assertEqual(1, find_short(test_string))

    def test_two_words(self):
        test_string = "tiger cat"
        self.assertEqual(3, find_short(test_string))

def find_short(input_string):
    """

    :param input_string:
    :return:
    """
    length_list = [len(word) for word in input_string.split(' ')]
    return min(length_list)

if __name__ == '__main__':
    unittest.main()
