import unittest
from collections import Counter


class MyTestCase(unittest.TestCase):
    def test_empty_string(self):
        empty_string = ""
        result = numerics(empty_string)
        self.assertEqual(result, "")

    def test_single_letter(self):
        result = numerics("a")
        self.assertEqual(result, "1")

    def test_single_occurrence_letters(self):
        test_string = "cat"
        result = numerics(test_string)
        self.assertEqual(result, "111")

    def test_multiple_occurrances_of_a_letter(self):
        test_string = "chatter"
        result = numerics(test_string)
        self.assertEqual(result, "1111211")

    def test_basic_hello_world(self):
        test_string = "Hello, World!"
        result = numerics(test_string)
        self.assertEqual(result, "1112111121311")

    def test_full_sentence(self):
        test_string = "Hello, World! It's me, JomoPipi!"
        result = numerics(test_string)
        self.assertEqual(result, "11121111213112111131224132411122")

    def test_single_repeated_letter(self):
        test_string = "aaaaaaaaaaaa"
        result = numerics(test_string)
        self.assertEqual(result, "123456789101112")

def numerics(input_string):
    """

    :param input_string:
    :return: string of count of letter occurrance
    """
    output_string = ""
    letter_counter = Counter()

    for letter in input_string:
        letter_counter[letter] += 1
        output_string += str(letter_counter[letter])

    return output_string



if __name__ == '__main__':
    unittest.main()
