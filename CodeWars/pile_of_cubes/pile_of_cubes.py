import unittest


class TestCubePile(unittest.TestCase):

    def test_two(self):
        parameter = 2
        result = find_nb(parameter)
        self.assertEqual(-1, result)

    def test_three(self):
        parameter = 3
        result = find_nb(parameter)
        self.assertEqual(-1, result)

    def test_nine(self):
        parameter = 9
        result = find_nb(parameter)
        self.assertEqual(2, result)

    def test_full_case_affirmative(self):
        parameter = 1071225
        result = find_nb(parameter)
        self.assertEqual(45, result)

def find_nb(total_volume, dimension_count=3):
    """

    :param total_volume:
    :return:
    """
    current_volume = 0
    block_count = 1

    while current_volume < total_volume:
        current_volume += block_count**dimension_count
        if current_volume == total_volume:
            return block_count
        block_count += 1
    return -1


if __name__ == '__main__':
    unittest.main()
