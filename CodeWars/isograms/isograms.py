import unittest
import re


class MyTestCase(unittest.TestCase):
    def test_empty_string(self):
        test_string = ""
        self.assertTrue(is_isogram(test_string))

    def test_non_string(self):
        test_string = 4.0
        with self.assertRaises(AssertionError):
            is_isogram(test_string)

    def test_basic_is_an_isomorph(self):
        test_string = 'cat'
        self.assertTrue(is_isogram(test_string))

    def test_basic_repeat_pattern(self):
        test_string = 'moose'
        self.assertFalse(is_isogram(test_string))

    def test_capitals_repeat_pattern(self):
        test_string = 'McCarthy'
        self.assertFalse(is_isogram(test_string))

    def test_numbers(self):
        test_string = 'mo0se'
        self.assertTrue(is_isogram(test_string))

def is_isogram(input_string):
    """
    Shows whether or not a string is an isogram, meaning no letters are repeated.
    :param input_string: Input string for testing.
    :return: Boolean-- true if string is an isogram.
    """
    assert isinstance(input_string, str), "Input must be a string"
    lower_string = str.lower(input_string)
    digitless_string = re.sub(r'[0-9]', '', lower_string)
    return len(set(digitless_string)) == len(digitless_string)


if __name__ == '__main__':
    unittest.main()
