import unittest


class TestArrayDiff(unittest.TestCase):
    def test_empty_array(self):
        first = []
        second = []
        result = array_diff(first, second)
        self.assertListEqual([], result)

    def test_single_number(self):
        first = [1]
        second = [1]
        result = array_diff(first, second)
        self.assertListEqual([], result)

    def test_multiple_numbers(self):
        first = [1,2,2,2,3]
        second = [2]
        result = array_diff(first, second)
        self.assertListEqual([1,3], result)

    def test_bad_input(self):
        first = 1.0
        second = 'antwerp'
        with self.assertRaises(AssertionError):
            array_diff(first, second)



def array_diff(a, b):
    """
    Removes the contents of a second array from a first array
    :param a: Main list
    :param b: Removal list
    :return: Main list less the removal list
    """
    assert (isinstance(a, list) and isinstance(b, list)), "Both inputs need to be lists."
    return [x for x in a if x not in b]


if __name__ == '__main__':
    unittest.main()
